const openBtn =  document.querySelectorAll(".open-modal-btn");
const modal = document.querySelector(".modal-overlay");
const closeBtn = document.querySelector(".close-modal-btn");
const spanAsiento = document.querySelector(".asiento");


function openModal(e) {
  modal.classList.remove("hide");
  spanAsiento.innerText = `${spanAsiento.innerText} ${e.target.innerText}`;
};

function closeModal(e, clickedOutside) {
  if(clickedOutside) {
    if(e.target.classList.contains("modal-overlay")) {
      modal.classList.add("hide");
    } 
  } else {
    modal.classList.add("hide");
  }
  spanAsiento.innerText = "Factura de asiento";
}

openBtn.forEach(element => {
 console.log(element);
  element.addEventListener("click", (e) => openModal(e));
});

modal.addEventListener("click", (e) => closeModal(e, true));
closeBtn.addEventListener("click", closeModal);
